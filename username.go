package username

import (
	"errors"
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

var (
	ErrInvalidUsernameBase = errors.New("Invalid username base")
	ErrInvalidUsername     = errors.New("Invalid username")
	ErrUnexpected          = errors.New("Unexpected error")
)

const (
	Splitter       string = ":"
	UsernameMinLen int    = 3
	UsernameMaxLen int    = 40
)

var (
	UsernameBaseRegexp = fmt.Sprintf(`^(?:@)?[a-zA-Z0-9]{%d,%d}$`,
		UsernameMinLen, UsernameMaxLen)
	UsernameRegexp = fmt.Sprintf(`(?:@)[a-zA-Z0-9]{%d,%d}(?:%s[0-9]+)?$`,
		UsernameMinLen, UsernameMaxLen, Splitter)
)

// Implementation

type username struct {
	base string
	id   int
}

func (u *username) Get() string {
	return fmt.Sprintf("@%s%s%d", u.base, Splitter, u.id)
}
func (u *username) GetWithAt() string {
	return fmt.Sprintf("@%s%s%d", u.base, Splitter, u.id)
}
func (u *username) Base() string {
	return u.base
}
func (u *username) ID() int {
	return u.id
}

// Interface

// Username:
// (@)<base>:<id>
// @anderson == @anderson:0 == anderson
// @anderson:1 != @anderson
// @anderson:0 != @anderson:666 != @4nd3rs0n:0
type Username interface {
	// anderson:0
	Get() string
	// @anderson:0
	GetWithAt() string
	// anderson
	Base() string
	// 0
	ID() int
}

func NewUsername(base string, id int) (Username, error) {
	if match, err := regexp.MatchString(UsernameBaseRegexp, base); !match {
		if err != nil {
			return nil, err
		}
		return nil, ErrInvalidUsernameBase
	}
	return &username{
		base: base,
		id:   id,
	}, nil
}

// Parsing

func ParseUsername(in string) (Username, error) {
	if in == "" {
		return nil, nil
	}

	if matched, err := regexp.MatchString(UsernameRegexp, in); !matched {
		if err != nil {
			return nil, err
		}
		return nil, ErrInvalidUsername
	}

	if in[0] == '@' {
		in = in[1:]
	}

	unameSlice := strings.Split(in, Splitter)

	unameBase := unameSlice[0]
	var unameID int
	var err error

	if len(unameSlice) < 2 {
		unameID = 0
	} else {
		unameID, err = strconv.Atoi(unameSlice[1])
		if err != nil {
			return nil, err
		}
	}

	return NewUsername(unameBase, unameID)
}
